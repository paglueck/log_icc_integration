import hashlib

CONST_RID_LENGTH = 15


class Action:

    def __init__(self, timestamp, ip, uid, desc):
        self.timestamp = timestamp
        self.ip = ip
        self.uid = uid
        self.status = 200
        self.szero = 0
        self.desc = desc

    def generate_request_id(self):
        """
        Generates a request id for a log action from the timestamp the ip and action type
        :return: Returns the request id as string
        """
        return hashlib.md5(self.timestamp.isoformat() + self.ip + self.desc).hexdigest()[0:15]

    def __str__(self):
        """
        Custom string representation for an action - used for log entries
        :return: Returns a log entry string representation
        """
        return self.timestamp.isoformat()[0:23] + "Z" + " R{" + self.generate_request_id() + "} " + "IP{" + self.ip + "} " + "H{" + str(self.status) + "} " + "S{" + str(self.szero) + "} " + "U{" + self.uid + "} " + self.desc + " {}"