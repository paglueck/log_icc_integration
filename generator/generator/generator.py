import hashlib
import random
from datetime import datetime, timedelta
from actions import Action
from user import User


# Const vars - Interval for log entry deviation, length of user id
CONST_DEVIATION_UPPER = 4
CONST_DEVIATION_LOWER = 0
CONST_UID_LENGTH = 8


# Anomalies
CONST_ANOMALIE_BRUTEFORCE = {"LOGINFAIL": 0.2}
CONST_ANOMALIE_MAILACCESS = {"PW-RESET": 0.1, "EMAIL-CHANGE": 0.1, "PW-CHANGE": 0.1}
CONST_ANOMALIE_PWCHANGES = {"PW-CHANGE": 0.2}


# TODO: REFACTOR PARAMETERS INTO DICTIONARIES


def merge_logs_into_one(single_user_logs):
    """
    Merges multiple logs into one single log

    :param single_user_logs: List of log lists for different users
    :return: List of log entries of all users
    """
    merged_logs = {}
    safe_timedelta = timedelta(seconds=1)
    for user_log in single_user_logs:
        for log in user_log:
            for log_entry in log:
                raw_time = log_entry.split("Z")[0]
                formatted_time = datetime.strptime(raw_time, "%Y-%m-%dT%H:%M:%S")
                while formatted_time in merged_logs.keys():
                    formatted_time += safe_timedelta
                merged_logs[formatted_time] = log_entry
    merged_log_list = []
    for key in sorted(merged_logs):
        merged_log_list.append(merged_logs[key])
    return merged_log_list


def generate_usage_for_today_with_ips(ips, entries, distribution, anomalies, entry_deviation_lower, entry_deviation_upper):
    """
    Generate logs for given users (IPs) for today
    :param ips: IPs (users) logs should be generated for
    :param entries: Amount of actions executed by the user
    :param distribution: Distribution of the actions that have been executed
    :param anomalies: Anomalies to incorporate
    :param entry_deviation_lower: Lower deviation bound to the total entry amount
    :param entry_deviation_upper: Upper deviation bound to the total entry amount
    :return:
    """

    all_logs = []
    for ip in ips:
        timedist = random.randint(600,1300);
        all_logs.append(generate_logs_for_ip_user(ip, 1, entries, distribution, anomalies, 0, timedist,
                                                  entry_deviation_lower, entry_deviation_upper))
    return merge_logs_into_one(all_logs)


def generate_multiple_user_logs(num_of_users, days, entries, distribution, anomalies, dayoffset, entry_deviation_lower, entry_deviation_upper):
    """
    Generate logs for multiple users with the given parameters and random distribution over the "day"

    Parameters
    ----------
    :param num_of_users: Number of users logs should be generated for
    :param days: Number of days of log data
    :param entries: Amount of entries for each day
    :param distribution: Distribution of the actions done by the user
    :param anomalies: Anomalies which should be incorporated
    :param dayoffset: Amount of days the data should be shifted forward or backward in time

    :return: Returns a single array containing all the log entries
    """
    ips = []
    for i in range(num_of_users):
        new_ip = generate_ip()
        while new_ip in ips:
            new_ip = generate_ip()
        ips.append(new_ip)
    all_logs = map(lambda x : generate_logs_for_ip_user(x, days, entries, distribution, anomalies, dayoffset, 1300,
                                                        entry_deviation_lower, entry_deviation_upper), ips)
    return merge_logs_into_one(all_logs)


def generate_logs_for_ip_user(ip, days, entries, distribution, anomalies, day_offset, minutes_to_distribute, entry_deviation_lower, entry_deviation_upper):
    """
    Generates user behavior logs with the given parameters

    Parameters
    ----------
    :param ip: IP of the user
    :param days: Amount of days of logs that should be generated
    :param entries: Amount of entries each log should have in total
    :param distribution: Distribution of different actions the user executes {"LOGINS": 0,7, ...}
    :param anomalies: Anomalies that should be injected in the logs {"LOGINFAIL": 1, "BRUTEF": 0,..}
    :param day_offset: Amount of days the timestamps should be moved in the future or past (positive=future,
    negative=past)
    :param minutes_to_distribute: Amount of total minutes the actions should be distributed over (1300 is a good value
    to not float into the next day with the actions)
    :param entry_deviation_lower: Lower bound for the deviation to the total entry amount
    :param entry_deviation_upper: Upper bound for the deviation to the total entry amount

    Return
    -------
    :return: Returns an array of arrays with the log data, where each of the arrays contains one day of log entries
    ([[log_entry,...],..[]])
    """

    # Step 1 - Generate the user with the behavior parameters
    user = generate_user_with_ip(ip)

    # Step 2 - Randomly determine where to place the anomalie(s)
    anomaly_days = {}
    if anomalies:
        for anomaly in anomalies.keys():
            random_day = str(random.randint(0, days - 1))
            if random_day not in anomaly_days.keys():
                anomaly_days[random_day] = [anomaly]
            else:
                anomaly_days[random_day].append(anomaly)

    # Step 3 - Calculate amount and generate actions for each type
    logs = []

    # Get today's date and set it to midnight for the action timestamps
    action_time = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

    # Offset the date into the past or future
    action_time += timedelta(days=day_offset)

    for day in range(days):

        actions = []

        # Step 3.1 - Calculate amount of total entries + random deviation
        entries_random_deviation = random.randint(entry_deviation_lower, entry_deviation_upper)
        entries_total = entries + entries_random_deviation

        # Step 3.2 - Calculate amount of entries for each type and add to this day's actions
        for action_type in distribution:
            entries_for_type = int(distribution[action_type] * entries_total)
            for entry in range(entries_for_type):
                actions.append(Action("", user.ip, user.id, action_type))

        if str(day) in anomaly_days.keys():
            for anomaly in anomaly_days[str(day)]:
                anomaly_template = None
                if anomaly == "brute":
                    anomaly_template = CONST_ANOMALIE_BRUTEFORCE
                elif anomaly == "mail":
                    anomaly_template = CONST_ANOMALIE_MAILACCESS
                elif anomaly == "pw":
                    anomaly_template = CONST_ANOMALIE_PWCHANGES
                for action_type in anomaly_template.keys():
                    entries_for_type = int(anomaly_template[action_type] * entries_total)
                    for entry in range(entries_for_type):
                        actions.append(Action("", user.ip, user.id, action_type))

        # Step 3.3 - Shuffle the array
        random.shuffle(actions)

        # Step 3.4 - Calculate timestamps and add to the actions
        # Add current day as offset to change days
        timestamp_base = action_time + timedelta(days=day)

        # Calculate minute offset to distribute the actions only over this day
        action_time_offset = minutes_to_distribute / len(actions)

        # Give each action a timestamp and increment the time by the time offset
        for action in actions:
            action.timestamp = timestamp_base
            timestamp_base += timedelta(minutes=action_time_offset)

        # Replace each action with its string representation
        actions = map(lambda a: str(a), actions)

        # Add the new log entries to the log array
        logs.append(actions)

    return logs


def generate_logs_for_random_user(days, entries, distribution, anomalies, day_offset, minutes_to_distribute, entry_deviation_lower, entry_deviation_upper):
    """
    Generates user behavior logs with the given parameters for a random user

    Parameters
    ----------
    :param days: Amount of days of logs that should be generated
    :param entries: Amount of entries each log should have in total
    :param distribution: Distribution of different actions the user executes {"LOGINS": 0,7, ...}
    :param anomalies: Anomalies that should be injected in the logs {"LOGINFAIL": 1, "BRUTEF": 0,..}
    :param day_offset: Amount of days the timestamps should be moved in the future or past (positive=future,
    negative=past)
    :param minutes_to_distribute: Amount of total minutes the actions should be distributed over (1300 is a good value
    to not float into the next day with the actions)
    :param entry_deviation_lower: Lower bound for the deviation to the total entry amount
    :param entry_deviation_upper: Upper bound for the deviation to the total entry amount

    Return
    -------
    :return: Returns an array of arrays with the log data, where each of the arrays contains one day of log entries
    ([[log_entry,...],..[]])
    """
    return generate_logs_for_ip_user(generate_ip(), days, entries, distribution, anomalies, day_offset, minutes_to_distribute, entry_deviation_lower, entry_deviation_upper)


def generate_user_with_ip(ip):
    '''
    Creates an user with a given IP and generates a user ID from the IP

    :param ip: User's IP as string

    :return: Returns the object representing the user containing IP and ID
    '''
    user_ip = ip
    user_id = generate_uid(user_ip)
    return User(user_ip, user_id)


def generate_ip():
    '''
    Generate a random IP Address (Unavailable IP addresses are ignored and will still be generated)

    :return: Returns the generated IP
    '''
    return str(random.randint(0, 255)) + "." + str(random.randint(0, 255)) + "." + str(
        random.randint(0, 255)) + "." + str(random.randint(0, 255))


def generate_uid(ip):
    """
    Generates a user's ID from the given IP address

    :param ip: IP address string

    :return: Returns the corresponding user ID
    """
    return hashlib.md5(ip.encode('utf-8')).hexdigest()[0:CONST_UID_LENGTH]





