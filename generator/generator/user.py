class User:

    def __init__(self, ip, id):
        self.ip = ip
        self.id = id

    def __str__(self):
        return "User with IP: " + self.ip + " and UID: " + self.id
