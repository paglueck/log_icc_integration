import requests
import json
from flask import Flask, render_template, request, session, flash, redirect, url_for
from functools import wraps

app = Flask(__name__)
CONST_SERVER_PORT = 5000
app.config.update(dict(
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
    Pseudo login for demo
    :return: Returns error login page if there was an error, returns the admin page if successful
    """
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('admin'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    """
    Logout
    :return: Returns index page
    """
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('index'))


def requires_auth(f):
    """
    Decorator to signal that login is needed to use that path
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        if not session.get('logged_in'):
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated


@app.route('/')
def index():
    """
    Index route to the landing page
    """
    return render_template('index.html')


@app.route('/anomalies')
def anomalies():
    """
    Fetch all anomalies and display them
    """
    result = requests.get("http://127.0.0.1:8000/anomalies")
    return render_template('anomalies.html', anomalies=result.json(), active_page='anomalies')


@app.route('/anomalies/today')
def anomalies_today():
    """
    Fetch all anomalies of today and display them
    """
    result = requests.get("http://127.0.0.1:8000/anomalies/today")
    return render_template('anomalies.html', anomalies=result.json(), active_page='anomalies/today')


@app.route('/anomalies/weekly')
def anomalies_weekly():
    """
    Fetch all anomalies of this week and display them
    """
    result = requests.get("http://127.0.0.1:8000/anomalies/weekly")
    return render_template('anomalies.html', anomalies=result.json(), active_page='anomalies/weekly')


@app.route('/anomalies/custom')
def anomalies_custom():
    """
    Show elements for a custom anomaly search
    """
    return render_template('anomalies.html', active_page='anomalies/custom')


@app.route('/anomalies/custom/date')
def anomalies_custom_date():
    """
    Fetch all anomalies for a custom time frame and display them
    """
    date_from = request.args.get('fromDate', default=None, type=None)
    date_to = request.args.get('toDate', default=None, type=None)
    date_payload = {'from': date_from, 'to': date_to}
    anomalies = requests.get("http://127.0.0.1:8000/anomalies/custom/date", date_payload)
    return render_template('anomalies.html', anomalies=anomalies.json(), active_page='anomalies/custom/result')


@app.route('/anomalies/custom/user')
def anomalies_custom_user():
    """
    Fetch all anomalies for a user and display them
    """
    user = request.args.get('user', default=None, type=None)
    user_payload = {'user': user}
    anomalies = requests.get("http://127.0.0.1:8000/anomalies/custom/user", user_payload)
    return render_template('anomalies.html', anomalies=anomalies.json(), active_page='anomalies/custom/result')


@app.route('/anomalies/custom/userdate')
def anomalies_custom_userdate():
    """
    Fetch all anomalies for a user and a custom time frame and display them
    """
    date_from = request.args.get('tfu-fromDate', default=None, type=None)
    date_to = request.args.get('tfu-toDate', default=None, type=None)
    user = request.args.get('tfu-user', default=None, type=None)
    userdate_payload = {'from': date_from, 'to': date_to, 'user': user}
    anomalies = requests.get("http://127.0.0.1:8000/anomalies/custom/userdate", userdate_payload)
    return render_template('anomalies.html', anomalies=anomalies.json(), active_page='anomalies/custom/result')


@app.route('/anomalies/merge/<int:anomid>')
def anomaly_merge_by_id(anomid):
    """
    Route to merge the anomalous behavior with the history of the user
    :param anomid: The ID of the anomaly to merge
    """
    anomaly_payload = {'anomaly': anomid}
    result = requests.put("http://127.0.0.1:8000/anomalies/merge/" + str(anomid))
    print "Merge"
    print result.text
    return redirect(url_for('anomalies'))


@app.route('/anomalies/resolve/<int:anomid>')
def anomaly_resolve_by_id(anomid):
    """
    Route to delete a resolved anomaly
    :param anomid: The ID of the anomaly to delete
    """
    result = requests.delete("http://127.0.0.1:8000/anomalies/resolve/" + str(anomid))
    return redirect(url_for('anomalies'))


@app.route('/anomalies/<int:anomid>')
def anomaly_by_id(anomid):
    """
    Route to display information for a specific anomaly
    :param anomid:
    :return:
    """
    anomaly = requests.get("http://127.0.0.1:8000/anomalies/" + str(anomid))
    return render_template('anomalies.html', anomalies=[anomaly.json()], active_page='anomalies/custom/result')

@app.route('/ubh')
def ubh():
    """
    Fetch all ubhs and display them
    """
    raw_ubhs = requests.get("http://127.0.0.1:8000/ubh")
    ubhs = raw_ubhs.json()
    new_ubhs = []
    for user in ubhs:
        users_logs = []
        for log in json.loads(user[1]):
            current_log = []
            for log_entry in log:
                current_log.append(log_entry)
            users_logs.append(current_log)
        new_ubhs.append([user[0], users_logs])
    return render_template('ubh.html', ubhs=new_ubhs)


@app.route('/admin')
@requires_auth
def admin():
    """
    Admin page
    """
    return render_template('admin.html')


@app.route('/admin/demo')
def start_demo():
    """
    Starts the demo and redirects to anomaly page
    """
    requests.get("http://127.0.0.1:8000/demo")
    return redirect(url_for('anomalies'))


@app.route('/admin/pdemo')
def prepare_demo():
    """
    Prepares a demo run
    """
    requests.get("http://127.0.0.1:8000/prepare")
    return render_template('admin.html', prepared='True')


if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded= True, port=CONST_SERVER_PORT, debug=True) #host='0.0.0.0'