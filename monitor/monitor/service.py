from . import utils
from nameko.timer import timer
from nameko.rpc import rpc, RpcProxy

# Microservice names
CONST_SERVICE_NAME = 'monitor_service'
CONST_SERVICE_NAME_ES_DATA = 'esdata_service'
CONST_SERVICE_NAME_ANOMALY = 'anomaly_service'
CONST_SERVICE_NAME_HUB = 'hub_service'

# Elasticsearch cluster address
CONST_ELASTIC_ADDRESS = "http://localhost:9200"

# Saved log entry path
CONST_PATH_LAST = 'last.txt'

# Timer interval in seconds
CONST_TIMER_INTERVAL = 10


class MonitorService:
    """
    Microservice that monitors elasticsearch for new log entries of the current day
    """

    # Name the microservice reacts to while listening for calls
    name = CONST_SERVICE_NAME

    # Microservice dependencies
    es = RpcProxy(CONST_SERVICE_NAME_ES_DATA)
    adc = RpcProxy(CONST_SERVICE_NAME_ANOMALY)
    hub = RpcProxy(CONST_SERVICE_NAME_HUB)
    logger = RpcProxy('logging_service')

    @rpc
    def fetch_new_entry_users(self, last_timestamp, recent_timestamp):
        """
        Fetch the new users between the two timestamps
        :param last_timestamp: Starting timestamp
        :param recent_timestamp: Ending timestamp
        :return: Returns the users as list or an empty list if there are none
        """
        new_entries = self.es.get_todays_entries_between_timestamps(last_timestamp, recent_timestamp)
        #print "Amount of new entries: %d" % len(new_entries)
        #if len(new_entries) == 0:
        #    print last_timestamp
        #    print recent_timestamp
        if new_entries:
            return utils.identify_users(new_entries)
        return []

    @rpc
    def anomaly_analysis_check(self, userid):
        """
        Checks the user and his behavior for the current day for anomalies if possible

        :param userid: ID of the user to check
        :return: Returns if there is an anomaly (True) or not (False)
        """
        # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(M)", "Check user %s for anomaly detection" % userid)
        # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(1)", "Fetching user %s entries for the current day" % userid)
        todays_behavior = self.es.fetch_todays_entries(userid)
        if todays_behavior:

            # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(2)", "Attempting to load ubh for user %s" % userid)
            past_behavior = self.hub.load_ubh(userid)

            # If there is no ubh yet, fetch the data from elasticsearch
            if not past_behavior:
                # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(3)", "Attempting to extract ubh from historic data for user %s instead" % userid)
                past_behavior = self.es.get_historic_data(userid, 7)
                self.hub.store_ubh(userid, past_behavior)

            avg_actions = utils.calculate_avg_actions(past_behavior)
            if len(todays_behavior) >= avg_actions:

                # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(4)", "Executing anomaly detection for user %s" % userid)
                detection_result = self.adc.detect_anomalies_with_ubh_using_boa(userid, past_behavior, todays_behavior)
                if detection_result is None:
                    print "Anomaly Detection was not possible"
                    self.hub.update_ubh(userid, todays_behavior)
                    return False
                if not detection_result:
                    print "No anomaly found"
                    self.hub.update_ubh(userid, todays_behavior)
                return detection_result
            # else:
            # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(4)", "Not enough entries of user %s for ad for current day" % userid)
        return False

    @timer(interval=CONST_TIMER_INTERVAL)
    def check_for_new_entries(self):
        """
        Periodically checks the elasticsearch cluster for new log entries and checks if anomaly detection needs to be
        run for certain users
        """
        if utils.is_elastic_availability(CONST_ELASTIC_ADDRESS):

            # self.logger.log(CONST_SERVICE_NAME, "MON-LFNL(M)", "Checking for new logs...")
            print "Looking for new logs...."

            last_read_line = utils.get_last_read_log_line(CONST_PATH_LAST)
            # self.logger.log(CONST_SERVICE_NAME, "MON-LFNL(1)", "Fetching todays recent entry")
            most_recent_line = self.es.get_todays_most_recent_entry()

            if most_recent_line is not False:

                if last_read_line != most_recent_line and last_read_line != "":
                    print "Found new data..."
                    # self.logger.log(CONST_SERVICE_NAME, "MON-LFNL(2)", "Found new log entries - continue")
                    from_timestamp = utils.get_timestamp(last_read_line)
                    to_timestamp = utils.get_timestamp(most_recent_line)
                    # self.logger.log(CONST_SERVICE_NAME, "MON-LFNL(3)", "Fetching users for the new log entries")
                    users = self.fetch_new_entry_users(from_timestamp, to_timestamp)
                    # self.logger.log(CONST_SERVICE_NAME, "MON-LFNL(4)", "Checking each user for possible execution of anomaly detection")
                    for user in users:
                        self.anomaly_analysis_check(user)
                    # self.check_new_log_data()
                # else:
                    # print "No new data..."
                utils.update_last_read_log_line(CONST_PATH_LAST, most_recent_line)
        else:
            print "elastic unavailable"

    # ------------------------------------------------- #
    #            Only for evaluation purposes           #
    # ------------------------------------------------- #

    @rpc
    def anomaly_analysis_check_for_day(self, userid, timestamp):
        """
        Checks the user and his behavior for the given day for anomalies if possible

        :param userid: ID of the user to check
        :param timestamp: Timestamp for checking
        :return: Returns if there is an anomaly (True) or not (False)
        """
        # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(M)", "Check user %s for anomaly detection" % userid)
        # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(1)", "Fetching user %s entries for the current day" % userid)
        todays_behavior = self.es.fetch_days_entries(userid, timestamp)
        if todays_behavior:

            # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(2)", "Attempting to load ubh for user %s" % userid)
            past_behavior = self.hub.load_ubh(userid)

            # If there is no ubh yet, fetch the data from elasticsearch
            if not past_behavior:
                # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(3)", "Attempting to extract ubh from historic data for user %s instead" % userid)
                past_behavior = self.es.get_historic_data_v2(userid, 7, timestamp)
                if past_behavior:
                    self.hub.store_ubh(userid, past_behavior)
                else:
                    return None

            avg_actions = utils.calculate_avg_actions(past_behavior)
            if len(todays_behavior) >= avg_actions:

                # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(4)", "Executing anomaly detection for user %s" % userid)
                detection_result = self.adc.detect_anomalies_with_ubh_using_boa(userid, past_behavior, todays_behavior)
                if detection_result is None:
                    print "Anomaly Detection was not possible"
                    self.hub.update_ubh(userid, todays_behavior)
                    return False
                if not detection_result:
                    self.hub.update_ubh(userid, todays_behavior)
                return detection_result
            # else:
                # self.logger.log(CONST_SERVICE_NAME, "MON-AAC(4)", "Not enough entries of user %s for ad for day %s" % (userid, day))
        return False
