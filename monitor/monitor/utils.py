import requests
from os import path


def is_elastic_availability(address):
    """
    Check if elasticsearch is up and running

    :return: Returns True if elasticsearch is up, False if not
    """

    try:
        r = requests.get(address)
    except requests.exceptions.ConnectionError:
        # Elasticsearch not up
        return False

    # Elasticsearch is up
    return True


def get_last_read_log_line(filepath):
    """
    Fetch the saved newest log line that was last found in the elasticsearch cluster
    :return: Returns the log line that was last fetched from the elasticsearch cluster
    """
    if path.exists(filepath):
        with open(filepath, "r") as r:
            return r.read()
    else:
        return ""


def update_last_read_log_line(filepath, line):
    """
    Update the newest log line that was last found in the elasticsearch cluster
    :return:
    """
    with open(filepath, "w") as f:
        f.write(line)


def get_user(log_entry):
    """
    Get user from a log entry

    :param log_entry: Log entry the user should get extracted from

    :return: Returns the user's ID
    """
    return log_entry.split("U{")[1].split("}")[0]


def group_log_by_user(log):
    """
    Transform a log array with entries of multiple users in a dictionary with all users as keys and their log entries as
    an array as value

    :param log: Array containing log entries of multiple users

    :return: Dictionary with all users and their log entries as arrays
    """
    user_dict = {}
    for log_entry in log:
        user = get_user(log_entry)
        if user not in user_dict.keys():
            user_dict[user] = []
        user_dict[user].append(log_entry)
    return user_dict


def calculate_avg_actions(past_behavior):
    """
    Calculates the avg amount of actions for a behavior history
    :param past_behavior: The past behavior of a user
    :return: Returns the amount of avg actions (int)
    """
    total_actions = 0
    for log in past_behavior:
        total_actions += len(log)
    return total_actions / len(past_behavior)


def identify_users(new_entries):
    """
    Finds all users in a list of log entries

    :param new_entries: List of log entries
    :return: List of users
    """
    users = []
    for log in new_entries:
        userid = get_user(log)
        if userid not in users:
            users.append(get_user(log))
    return users


def get_timestamp(log_entry):
    """
    Returns the timestamp of a log entry
    :param log_entry: Log entry
    :return: Extracted timestamp
    """
    return log_entry.split(" ")[0]