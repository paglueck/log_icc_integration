import json
from datetime import datetime, timedelta
from nameko.rpc import RpcProxy
from nameko.web.handlers import http

# Constant vars - names of needed services
CONST_SERVICE_NAME = 'bff_service'
CONST_SERVICE_NAME_ADC = 'anomaly_service'
CONST_SERVICE_NAME_HUB = 'hub_service'
CONST_SERVICE_NAME_ES_DATA = 'esdata_service'


class BFFService:
    """
    Microservice for tieing together frontend and anomaly/ubh databases (additionally controls the demo mode)
    """

    # Name the microservice reacts to while listening for calls
    name = CONST_SERVICE_NAME

    # Necessary microservice dependencies
    adc = RpcProxy(CONST_SERVICE_NAME_ADC)
    es = RpcProxy(CONST_SERVICE_NAME_ES_DATA)
    hub = RpcProxy(CONST_SERVICE_NAME_HUB)

    # ------------------------------------------------- #
    #   HTTP-API endpoints to query anomalies and UBH   #
    # ------------------------------------------------- #

    @http('GET', '/anomalies')
    def get_all_anomalies(self, request):
        """
        Return all anomalies from the database

        :return: Returns a json representation of all anomalies
        """
        anomalies = self.adc.load_anomalies()
        return json.dumps(anomalies)

    @http('GET', '/anomalies/today')
    def get_todays_anomalies(self, request):
        """
        Return today's anomalies

        :return: Return a json representation of today's anomalies
        """
        today = datetime.now().replace(hour=0, minute=0, second=0)
        tomorrow = today + timedelta(hours=23, minutes=59, seconds=59)
        anomalies = self.adc.load_anomalies_for_timeframe(str(today), str(tomorrow))
        print today
        print tomorrow
        return json.dumps(anomalies)

    @http('GET', '/anomalies/weekly')
    def get_weekly_anomalies(self, request):
        """
        Get this week's anomalies

        :return: Return a json representation of this week's anomalies
        """
        day = datetime.now()
        week_start = (day - timedelta(days=day.weekday())).replace(hour=0, minute=0, second=0)
        week_end = (week_start + timedelta(days=6)).replace(hour=23, minute=59, second=59)
        anomalies = self.adc.load_anomalies_for_timeframe(str(week_start), str(week_end))
        return json.dumps(anomalies)

    @http('GET', '/anomalies/custom/date')
    def get_custom_date_anomalies(self, request):
        """
        Return all anomalies for a custom timeframe

        :return: Return a json representation of all anomalies in the custom timeframe
        """
        datepicker_format = '%Y-%m-%d'
        date_from = request.args.get('from')
        datetime_from = datetime.strptime(date_from, datepicker_format).replace(hour=0, minute=0, second=0)
        date_to = request.args.get('to')
        datetime_to = datetime.strptime(date_to, datepicker_format).replace(hour=23, minute=59, second=59)
        anomalies = self.adc.load_anomalies_for_timeframe(str(datetime_from), str(datetime_to))
        return json.dumps(anomalies)

    @http('GET', '/anomalies/custom/user')
    def get_custom_user_anomalies(self, request):
        """
        Return all anomalies for a custom user

        :return: Return a json representation of all anomalies for the specified user
        """
        user = request.args.get('user')
        anomalies = self.adc.load_anomalies_for_user(user)
        return json.dumps(anomalies)

    @http('GET', '/anomalies/custom/userdate')
    def get_custom_userdate_anomalies(self, request):
        """
        Return all anomalies for a custom user and timeframe

        :return: Return a json representation of all anomalies for the user and timeframe specified in the request
        """
        datepicker_format = '%Y-%m-%d'
        date_from = request.args.get('from')
        datetime_from = datetime.strptime(date_from, datepicker_format).replace(hour=0, minute=0, second=0)
        date_to = request.args.get('to')
        datetime_to = datetime.strptime(date_to, datepicker_format).replace(hour=23, minute=59, second=59)
        user = request.args.get('user')
        anomalies = self.adc.load_anomalies_for_user_timeframe(user, str(datetime_from), str(datetime_to))
        return json.dumps(anomalies)

    @http('GET', '/anomalies/<int:anomid>')
    def get_anomaly_by_id(self, request, anomid):
        """
        Load anomaly from the database

        :param request: Request from the frontend
        :param anomid: ID of the anomaly
        :return: Return the anomaly if it exists, False else
        """
        anomaly = self.adc.load_anomaly_by_id(anomid)
        if anomaly:
            return json.dumps(anomaly)
        return False

    @http('PUT', '/anomalies/merge/<int:anomid>')
    def merge_anomaly_by_id(self, request, anomid):
        """
        Merges an anomalous log with the ubh of a user - usually done when anomaly was false positive

        :param request: Request from the frontend
        :param anomid: ID of the anomaly
        :return: Return True if successful, False else
        """
        anomaly = self.adc.load_anomaly_by_id(anomid)
        update_result = self.hub.update_ubh(anomaly[2], anomaly[5])  # anomaly[2] = user id, anomaly[5] = anomalous log
        print update_result
        if update_result:
            delete_result = self.adc.delete_by_id(anomid)
            return json.dumps(delete_result)
        return json.dumps(False)

    @http('DELETE', '/anomalies/resolve/<int:anomid>')
    def delete_anomaly_by_id(self, request, anomid):
        """
        Delete an anomaly because it was inspected and confirmed as such

        :param request: Request of the frontend
        :param anomid: ID of the anomaly
        :return: Return the result of the delete operation
        """
        result = self.adc.delete_by_id(anomid)
        return json.dumps(result)

    @http('GET', '/ubh')
    def get_all_ubhs(self, request):
        """
        Return all UBHs (user behavior history) from the database

        :return: Return a json representation of all UBHs
        """
        return json.dumps(self.hub.load_all_ubhs())

    @http('POST', '/anomalies/merge')
    def merge_log_with_ubh(self, request):
        """
        Merges the behavior of an anomaly with the history of the user and deletes the anomaly

        :return: Returns the result of the operation, True if successful, False else
        """
        userid = request.args.get('user')
        anomid = request.args.get('anomaly')
        return self.hub.merge_ubh(userid, anomid)

    @http('DELETE', '/anomalies/resolve')
    def resolve_anomaly(self, request):
        """
        Deletes a resolved anomaly from the database

        :return: Returns the result of the database operation, True if successful, False else
        """
        anomid = request.args.get('anomaly')
        return self.adc.resolve_anomaly_by_id(anomid)

    # ------------------------------------------------- #
    #      Only for demonstration/testing purposes      #
    # ------------------------------------------------- #

    @http('GET', '/prepare')
    def prepare_demo(self, request):
        """
        DEMO PREPARATION
        """
        self.es.clean_index()
        self.adc.delete_all_anomalies()
        self.hub.delete_all_ubhs()
        self.es.demo_data_upstream(10)

    @http('GET', '/demo')
    def start_demo(self, request):
        """
        START DEMO
        """
        self.es.start_simulating_usage()
