from nameko.rpc import rpc
from datetime import datetime

# Constant vars
CONST_SERVICE_NAME = 'logging_service'
CONST_LOGFILE_PREFIX = 'report'
CONST_LOGFILE_TYPE = '.log'


class LoggingService:
    """
    Simple logging service
    """

    name = CONST_SERVICE_NAME

    @rpc
    def log(self, service, code, line):
        """
        Writes a new line into the log of the current day

        :param service: Service that called to log an event
        :param code: Code of the operation that should get logged
        :param line: Text that should get logged
        :return:
        """
        today = str(datetime.now().date())
        with open(CONST_LOGFILE_PREFIX + today + CONST_LOGFILE_TYPE, "a") as f:
            f.write("%s: [%s] (%s) %s \n" % (datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), service, code, line))
