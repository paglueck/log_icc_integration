import sqlite3
import json
from nameko.rpc import rpc, RpcProxy

# Constant vars
CONST_SERVICE_NAME = 'hub_service'
CONST_DB_FILENAME = 'HUB.db'
CONST_UBH_MAX_SIZE = 7 # Maximum amount of "days" in ubh


class HUBService:
    """
    Microservice that offers functionality to store the user's past behavior (UBH = User Behavior History)
    """

    # Name the microservice reacts to while listening for calls
    name = CONST_SERVICE_NAME

    logger = RpcProxy('logging_service')

    @rpc
    def check_user_ubh_exists(self, uid):
        """
        Check if there already is a UBH stored for a given user

        :param uid: User ID of the user that should be checked for
        :return: Returns True if a UBH already exists, False if there's none
        """
        db = sqlite3.connect(CONST_DB_FILENAME)
        db_cursor = db.cursor()
        if db_cursor.execute("SELECT * FROM users WHERE userid == ?", (uid,)).fetchone() is not None:
            db.close()
            return True
        db.close()
        return False

    @rpc
    def store_ubh(self, uid, ubh):
        """
        Store a new UBH for a user given by his user ID

        :param uid: User's id
        :param ubh: Log data for the UBH of the user

        :return: Returns True if the insertion was successful, False if there already exists an UBH for this user
        """
        db = sqlite3.connect(CONST_DB_FILENAME)
        db_cursor = db.cursor()
        if db_cursor.execute("SELECT * FROM users WHERE userid == ?", (uid,)).fetchone() is not None:
            db.close()
            # self.logger.log(CONST_SERVICE_NAME,"HUB-S-E" ,"UBH already exists for user %s" % uid)
            return False
        db_cursor.execute("INSERT INTO users VALUES (?, ?)", (uid, json.dumps(ubh),))
        db.commit()
        db.close()
        # self.logger.log(CONST_SERVICE_NAME,"HUB-S-S" ,"Successfully stored ubh for user %s" % uid)
        return True

    @rpc
    def load_ubh(self, uid):
        """
        Load an UBH for a given user ID from the database

        :param uid: User's ID
        :return: Returns the user's UBH if one exists and False if no UBH exists yet for that user
        """
        db = sqlite3.connect(CONST_DB_FILENAME)
        db_cursor = db.cursor()
        db_cursor.execute("SELECT uhb FROM users WHERE userid == ?", (uid,))
        result = db_cursor.fetchone()
        db.close()
        print result
        if result is None:
            # self.logger.log(CONST_SERVICE_NAME, "HUB-L-E","No ubh for user %s was found" % uid)
            return []
        else:
            user_ubh = json.loads(result[0])
            # self.logger.log(CONST_SERVICE_NAME, "HUB-L-S", "Successfully loaded ubh for user %s" % uid)
            return user_ubh
        return False

    @rpc
    def update_ubh(self, uid, ubh_update):
        """
        Update a user's UBH with a new entry - Eliminates the oldest entry of the user's UBH and pushes the update into
        the UBH
        :param uid: User's ID
        :param ubh_update: New action history that should be inserted into the UBH
        :return: Returns True when the updating was successful, False if there was an error
        """
        ubh = self.load_ubh(uid)
        if ubh:
            print ubh
            if len(ubh) >= CONST_UBH_MAX_SIZE:
                del ubh[0]
            ubh.append(ubh_update)
            db = sqlite3.connect(CONST_DB_FILENAME)
            db_cursor = db.cursor()
            db_cursor.execute("UPDATE users SET 'uhb'=? WHERE userid == ?", (json.dumps(ubh), uid,))
            db.commit()
            db.close()
            # self.logger.log(CONST_SERVICE_NAME, "HUB-U-S", "Successfully updated ubh of user %s" % uid)
            return True
        # self.logger.log(CONST_SERVICE_NAME, "HUB-U-E-1", "Couldn't update ubh of user %s - not existing" % uid)
        return False

    @rpc
    def load_all_ubhs(self):
        """
        Load all existing UBHs for frontend displaying purposes

        :return: Returns an array with all existing UBHs
        """

        db = sqlite3.connect(CONST_DB_FILENAME)
        db_cursor = db.cursor()
        db_cursor.execute("SELECT userid, uhb FROM users")
        ubhs = db_cursor.fetchall()
        db.close()
        if ubhs is None or len(ubhs) == 0:
            return []
        else:
            return ubhs

    @rpc
    def delete_all_ubhs(self):
        """
        Deletes all existing UBHs from the database
        """
        db = sqlite3.connect(CONST_DB_FILENAME)
        db_cursor = db.cursor()
        db_cursor.execute("DELETE FROM users")
        db.commit()
        db.close()


def check_hub_db():
    """
    Checks if the database table was already created, if not create it
    """
    db = sqlite3.connect(CONST_DB_FILENAME)
    db_cursor = db.cursor()
    db_cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='users'")
    if db_cursor.fetchone() is None:
        print("-- User Database created --")
        db_cursor.execute('''CREATE TABLE users (userid text, uhb text)''')
        db.commit()
    else:
        print("-- User Database already exists --")
    db.close()


# Check db in the beginning
check_hub_db()
