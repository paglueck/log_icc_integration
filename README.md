---------------------------------------------------------------------------
#iC Consult ML-IDS
---------------------------------------------------------------------------

## Setup

To run the framework there needs to be a running RabbitMQ Server (using Version 3.7.6) on http://localhost:5672
and a running Elasticsearch cluter

There's three options to get a running RabbitMQ-Server/Elasicsearch cluster quickly:

- Create a Docker container with the RabbitMQ / Elasticsearch image from Docker Hub
- Install RabbitMQ via Homebrew (```brew update``` > ```brew install rabbitmq```) and start with '/usr/local/sbin/rabbitmq-server'
- Download it from RabbitMQ / Elasticsearch (https://www.rabbitmq.com/download.html)

Run ```bash start.sh``` to start all the services on a mac

Docker implementation still missing, coming on 01.10.18...

