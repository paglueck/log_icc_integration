import numpy as np
import pytest
from anomaly.anomaly import bagofactions as boa


CONST_TEST_BAGS = []

@pytest.mark.parametrize(
    ('logs', 'expected'), [
    ([], []),

    ([['2018-05-13T23:00:11.792Z R{r2rqf3tgz43z} IP{123.123.123.123} H{200} S{0} U{r231r12r5} LOGIN {}']],
     [[1.0, 0.0, 0.0, 0.0, 0.0, 0.0]]),

    ([['2018-05-13T23:00:11.792Z R{r2rqf3tgz43z} IP{123.123.123.123} H{200} S{0} U{r231r12r5} LOGOUT {}']],
     [[0.0, 1.0, 0.0, 0.0, 0.0, 0.0]]),

    ([['2018-05-13T23:00:11.792Z R{r2rqf3tgz43z} IP{123.123.123.123} H{200} S{0} U{r231r12r5} PW-CHANGE {}']],
     [[0.0, 0.0, 1.0, 0.0, 0.0, 0.0]]),

    ([['2018-05-13T23:00:11.792Z R{r2rqf3tgz43z} IP{123.123.123.123} H{200} S{0} U{r231r12r5} LOGINFAIL {}']],
     [[0.0, 0.0, 0.0, 1.0, 0.0, 0.0]]),

    ([['2018-05-13T23:00:11.792Z R{r2rqf3tgz43z} IP{123.123.123.123} H{200} S{0} U{r231r12r5} EMAIL-CHANGE {}']],
     [[0.0, 0.0, 0.0, 0.0, 1.0, 0.0]]),

    ([['2018-05-13T23:00:11.792Z R{r2rqf3tgz43z} IP{123.123.123.123} H{200} S{0} U{r231r12r5} PW-RESET {}']],
     [[0.0, 0.0, 0.0, 0.0, 0.0, 1.0]]),

    ([['2018-05-13T23:00:11.792Z R{r2rqf3tgz43z} IP{123.123.123.123} H{200} S{0} U{r231r12r5} LOGIN {}'],
      ['2018-05-13T23:00:11.792Z R{r2rqf3tgz43z} IP{123.123.123.123} H{200} S{0} U{r231r12r5} LOGIN {}']],
     [[1.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1.0, 0.0, 0.0, 0.0, 0.0, 0.0]]),
   ]
)
def test_calc_bags(logs, expected):
    assert boa.calculate_bags(logs) == expected


@pytest.mark.parametrize(
   ('bags', 'no_of_bags', 'expected'), [
       ([], 0, ([], [])),
       ([], 0, ([], [])),
   ]
)
def test_calc_rand_vars(bags, no_of_bags, expected):
    assert boa.calculate_random_vars(bags, no_of_bags) == expected


@pytest.mark.parametrize(
   ('bags', 'no_of_bags', 'expected'), [
       ([], 0, ([], [])),
       ([], 0, ([], [])),
   ]
)
def test_calc_thresh():
    assert 0 == 0

