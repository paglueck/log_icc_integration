import sqlite3
import json
from datetime import datetime, timedelta

# Constant vars - database file
CONST_DB_FILENAME = 'anomalies.db'


def connect_to_db(path_to_db):
    """
    Connect to the database at the given path
    :param path_to_db: Path to the database
    :return: Returns a tuple of the database object and the database cursor
    """
    db = sqlite3.connect(path_to_db)
    db_cursor = db.cursor()
    return db, db_cursor


def store_anomaly(uid, cause, probability, logs):
    """
    Stores a new anomaly in the database

    Parameters
    ----------
    :param uid: User ID the anonmaly was found
    :param cause: Predicted cause of the anomaly
    :param probability: False positive probability
    :param logs: Log entries that caused the anomaly

    :return: Returns True if the anomaly was inserted successfully, False if there was an error
    """
    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("INSERT INTO anomalies(found_at, userid, cause, prob, logs) VALUES (datetime('now', 'localtime'), ?, ?, ?, ?)", (uid, cause, probability, json.dumps(logs), ))
    db[0].commit()
    db[0].close()
    return True


def load_anomalies():
    """
    Loads all existing anomalies for frontend displaying purposes

    :return: Returns an array with all the anomalies and their details
    """
    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("SELECT anom_id, datetime(found_at), userid, cause, prob, logs FROM anomalies ORDER BY anom_id DESC")
    anomalies = db[1].fetchall()
    db[0].close()
    return map(lambda x: get_anomaly_dict(x), anomalies)


def load_anomalies_for_id(anomaly_id):
    """
    Loads the anomaly for the given ID
    :param anomaly_id: ID of the anomaly to be loaded
    :return: Returns a list containing the details of the requested anomaly, empty array if the anomaly doesn't exist
    """
    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("SELECT anom_id, datetime(found_at), userid, cause, prob, logs FROM anomalies WHERE anom_id=?", (anomaly_id,))
    anomaly = db[1].fetchone()
    db[0].close()
    if anomaly:
        return get_anomaly_dict(anomaly)
    return []


def load_anomalies_for_user(uid):
    """
    Load the anomalies of a specific user with his user ID

    :param uid: User's ID

    :return: Returns an array with all anomalies for the corresponding user
    """
    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("SELECT anom_id, datetime(found_at), userid, cause, prob, logs FROM anomalies WHERE userid=? ORDER BY anom_id DESC", (uid,))
    anomalies = db[1].fetchall()
    db[0].close()
    return map(lambda x: get_anomaly_dict(x), anomalies)


def load_anomalies_for_timeframe(date_from, date_to):
    """
    Load all anomalies for a given timeframe
    (Date format; YYYY-mm-dd)

    Parameters
    ----------
    :param date_from: Start date
    :param date_to: End date

    :return: Returns an array containing all the anomalies for the given timeframe with all their details
    """
    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("SELECT anom_id, datetime(found_at), userid, cause, prob, logs FROM anomalies WHERE datetime(found_at) BETWEEN datetime(?) AND datetime(?) ORDER BY anom_id DESC", (date_from, date_to, ))
    anomalies = db[1].fetchall()
    db[0].close()
    return map(lambda x: get_anomaly_dict(x), anomalies)


def load_anomalies_for_user_timeframe(uid, date_from, date_to):
    """
    Loads all anomalies for a user in given timeframe
    (Date format; dd-MM-YYYY)
    Parameters
    ----------
    :param uid: User's ID
    :param date_from: Start date
    :param date_to: End date

    :return: Returns an array containing all the anomalies for the given timeframe with all their details
    """
    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("SELECT anom_id, datetime(found_at), userid, cause, prob, logs FROM anomalies WHERE userid=? AND datetime(found_at) BETWEEN datetime(?) AND datetime(?) ORDER BY anom_id DESC", (uid, date_from, date_to, ))
    anomalies = db[1].fetchall()
    db[0].close()
    return map(lambda x: get_anomaly_dict(x), anomalies)


def load_anomalies_for_last_hours(amount=1):
    """
    Loads all anomalies occurred in the last x hours (std: 1 hour)
    :param amount: Amount of hours of anomalies that should be loaded
    :return: Returns a list with all the anomalies occurred in those x hours
    """
    current_time_end = datetime.now()
    current_time_start = current_time_end - timedelta(hours=amount)
    print current_time_end
    print current_time_start
    format = '%Y-%m-%d %H:%M:%S'
    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("SELECT anom_id, found_at, userid, cause, prob, logs FROM anomalies WHERE found_at BETWEEN datetime(?) AND datetime(?) ORDER BY anom_id ASC", (current_time_start.strftime(format), current_time_end.strftime(format),))
    anomalies = db[1].fetchall()
    db[0].close()
    return map(lambda x: get_anomaly_dict(x), anomalies)


def delete_anomaly_by_id(anomid):
    """
    Deletes the specifiec anomaly from the database
    :param anomid: ID of the anomaly that should be deleted
    :return:
    """
    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("DELETE FROM anomalies WHERE anom_id == ?", (anomid, ))
    db[0].commit()
    db[0].close()
    return True


def delete_all_anomalies():
    """
    Clears the DB from all anomalies

    :return: Returns True if successful, False if there was an error
    """
    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("DELETE FROM anomalies")
    db[0].commit()
    db[0].close()
    return True


def get_anomaly_dict(db_anomaly):
    return {'id': db_anomaly[0], 'found_at': db_anomaly[1], 'userid': db_anomaly[2], 'cause': db_anomaly[3], 'fp_prob': db_anomaly[4], 'logs': [json.loads(db_anomaly[5])]}


def check_anomalies_db():
    """
    Checks if the necessary database table has already been created

    :return: Returns True if it already exists, False otherwise
    """

    db = connect_to_db(CONST_DB_FILENAME)
    db[1].execute("SELECT name FROM sqlite_master WHERE type='table' AND name='anomalies'")
    if db[1].fetchone() is None:
        print("-- Anomaly Database created --")
        db[1].execute('''CREATE TABLE anomalies (anom_id integer primary key, found_at datetime, userid text, cause text, prob float, logs text)''')
        db[0].commit()
    else:
        print("-- Anomaly Database already exists --")
    db[0].close()


def store_anomaly_old(day, uid, cause):
    """
    Stores a new anomaly in the database - Old Method for manual storing

    Parameters
    ----------
    :param day: Date the anomaly was found
    :param uid: User ID the anonmaly was found
    :param cause: Predicted cause of the anomaly

    :return: Returns True if the anomaly was inserted successfully, False if there was an error
    """
    db = connect_to_db(CONST_DB_FILENAME)
    format_str = '%Y-%m-%d %H:%M:%S'  # The format
    date_time = datetime.strptime(day, format_str)
    db[1].execute("INSERT INTO anomalies VALUES (?, ?, ?)", (date_time, uid, cause,))
    db[0].commit()
    db[0].close()
    return True