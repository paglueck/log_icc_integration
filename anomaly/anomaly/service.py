from nameko.rpc import rpc, RpcProxy
from datetime import datetime
from . import report
from . import bagofactions
from . import storage

# Constant vars - microservice names
CONST_SERVICE_NAME = 'anomaly_service'

# Constant vars - timer
CONST_TIMER_INTERVAL = 3600

# Constant vars - parameters for bag of actions approach
CONST_PARAM_ALPHA = 0.27


class AnomalyService:
    """
    Microservice offering the anomaly detection functionality
    """

    # Name the microservice reacts to, when listening for calls
    name = CONST_SERVICE_NAME

    logger = RpcProxy('logging_service')

    @rpc
    def detect_anomalies_with_ubh_using_boa(self, uid, ubh, log_to_check):
        """
        Anomaly detection RPC endpoint - real functionality

        Parameters
        ----------
        :param uid: ID of the user whose logs get checked for anomalies
        :param ubh: The historic behavior of the user
        :param log_to_check: The new behavior of the user which gets checked for deviation with the anomaly detection

        :return: Returns True if an anomaly was detected and False if none was detected
        """

        result = bagofactions.detect(ubh, [log_to_check], CONST_PARAM_ALPHA)
        if result is None:
            # self.logger.log(CONST_SERVICE_NAME, "ANOM-DET_BOA-E", "Matrix in singular state, no detection possible")
            return None
        if result[0]:
            causes = " ".join(str(c) for c in result[2])
            # self.logger.log(CONST_SERVICE_NAME, "ANOM-DET_BOA-R-(%s)" % cause_log, "Anomaly found for user %s because of %s (FP-Prob: %s)" % (uid, cause_log, str(result[1])))
            storage.store_anomaly(uid, causes, result[1], log_to_check)
            #report.send_mail_report(str(datetime.now()), uid, causes, [])  # Too many anomalies, cause spam :D
            return True
        else:
            # self.logger.log(CONST_SERVICE_NAME, "ANOM-DET_BOA-R-NOA", "No anomaly found for user %s" % uid)
            return False

    @rpc
    def load_anomalies(self):
        """
        Loads all existing anomalies for frontend displaying purposes

        :return: Returns an array with all the anomalies and their details
        """
        return storage.load_anomalies()

    @rpc
    def load_anomaly_by_id(self, anomid):
        return storage.load_anomalies_for_id(anomid)

    @rpc
    def load_anomalies_for_user(self, uid):
        """
        Load the anomalies of a specific user with his user ID

        :param uid: User's ID

        :return: Returns an array with all anomalies for the corresponding user
        """
        return storage.load_anomalies_for_user(uid)

    @rpc
    def load_anomalies_for_date(self, date):
        """
        Load all anomalies for a specific date

        :param date: Date the anomalies should be fetched for

        :return: Returns an array with all the anomalies for the given date with all their details
        """
        return storage.load_anomalies_for_date(date)

    @rpc
    def load_anomalies_for_timeframe(self, date_from, date_to):
        """
        Load all anomalies for a given timeframe
        (Date format; YYYY-mm-dd)

        Parameters
        ----------
        :param date_from: Start date
        :param date_to: End date

        :return: Returns an array containing all the anomalies for the given timeframe with all their details
        """
        return storage.load_anomalies_for_timeframe(date_from, date_to)

    @rpc
    def load_anomalies_for_user_timeframe(self, uid, date_from, date_to):
        """
        Load all anomalies for user in given timeframe
        (Date format; dd-MM-YYYY)
        Parameters
        ----------
        :param uid: User's ID
        :param date_from: Start date
        :param date_to: End date

        :return: Returns an array containing all the anomalies for the given timeframe with all their details
        """
        return storage.load_anomalies_for_user_timeframe(uid, date_from, date_to)

    @rpc
    def load_anomalies_for_last_hours(self, amount):
        """
        Loads all anomalies occurred in the last x hours (std: 1 hour)
        :param amount: Amount of hours of anomalies that should be loaded
        :return: Returns a list with all the anomalies occurred in those x hours
        """
        anomalies = storage.load_anomalies_for_last_hours(amount)
        return anomalies

    @rpc
    def delete_by_id(self, anomid):
        return storage.delete_anomaly_by_id(anomid)

    @rpc
    def delete_all_anomalies(self):
        """
        Clears the DB from all anomalies

        :return: Returns True if successful, False if there was an error
        """
        return storage.delete_all_anomalies()

    # ------------------------------------------------- #
    #               Only for testing purposes           #
    # ------------------------------------------------- #

    @rpc
    def detect_anomalies_using_boa(self, training, prediction):
        """
        Anomaly detection RPC endpoint - for evaluation/testing purposes

        Parameters
        ----------
        :param training: Raw user logs for training as array of string arrays. ([[string log entry,..],..[])
        :param prediction: Raw user logs for prediction as array of string arrays. ([[string log entry,..],..[])

        :return: Passes through the returned array of the detect method of the bagofactions.py module
        """
        return bagofactions.detect(training, prediction, CONST_PARAM_ALPHA)


# Check db in the beginning
storage.check_anomalies_db()
