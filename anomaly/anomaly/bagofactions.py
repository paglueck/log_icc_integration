import numpy as np
from scipy.stats.kde import gaussian_kde
from scipy.linalg import LinAlgError

'''
Implementation of the Bag of Actions approach

Summarized depiction:
- Create Bags of Actions from training data logs
- Calculate random vars for each Bag
- Estimate probability density function with random vars
- Calculate threshold with pdf and sensitivity factor alpha
- Create bags for prediction and calculate random var for that bag
- Use that random var as input for the pdf and compare with threshold
- >= Threshold = no anomaly, < Threshold = anomaly
'''

# Constant vars - action names, actions array - ! Add more Actions from the logs here !
CONST_ACTION_NAME_LOGIN = "LOGIN"
CONST_ACTION_NAME_LOGOUT = "LOGOUT"
CONST_ACTION_NAME_PWCHANGE = "PW-CHANGE"
CONST_ACTION_NAME_LOGINFAIL = "LOGINFAIL"
CONST_ACTION_NAME_MAILCHANGE = "EMAIL-CHANGE"
CONST_ACTION_NAME_PWRESET = "PW-RESET"

# ! Add more Actions from the logs here !
CONST_ACTIONS = [CONST_ACTION_NAME_LOGIN,
                 CONST_ACTION_NAME_LOGOUT,
                 CONST_ACTION_NAME_PWCHANGE,
                 CONST_ACTION_NAME_LOGINFAIL,
                 CONST_ACTION_NAME_MAILCHANGE,
                 CONST_ACTION_NAME_PWRESET]

CONST_PROB_RANGE_MIN = 0
CONST_PROB_RANGE_MAX = 100

CONST_LOG_DETAIL_SEPARATOR = " "


def calculate_bags(logs):
    """
    Preprocessing of log strings into raw bags of actions (raw meaning that they're not Numpy vectors yet)

    Parameters
    ----------
    :param logs: Array containing 0..n arrays (representing individual logs) with 1..n log entry strings ([[],[],..,[]])

    :return: Returns an array of arrays, each array in the general array contains the number of occurrences for the log
    actions. The number of arrays in the general array corresponds to the number of logs in the input array ([],..,[])
    """
    result_bags = []
    for log in logs:
        raw_bag = []
        for entry in log:
            entry_splitted = entry.split(CONST_LOG_DETAIL_SEPARATOR)
            raw_bag.append(entry_splitted[6])

        # ! Add more Actions from the logs here !
        result_bags.append([float(raw_bag.count(CONST_ACTION_NAME_LOGIN)),
                     float(raw_bag.count(CONST_ACTION_NAME_LOGOUT)),
                     float(raw_bag.count(CONST_ACTION_NAME_PWCHANGE)),
                     float(raw_bag.count(CONST_ACTION_NAME_LOGINFAIL)),
                     float(raw_bag.count(CONST_ACTION_NAME_MAILCHANGE)),
                     float(raw_bag.count(CONST_ACTION_NAME_PWRESET))])
    return result_bags


def calculate_random_vars(bag_vectors, no_of_bags, training_bag_vectors=None):
    """
    Calculation of the random variables for each bag in bag_vectors. If training_bag_vectors is not None
    then bag_vector's random variables get calculated with the training_bag_vectors (prediction).

    Formula for a random variable: x_n = (Sum of the distance of each bag with every other bag) / N
    Distance of two bags = |b_1 - b_2| = |(4,2,1) - (3,2,2)| = (1,0,1)

    :param bag_vectors: Bags the random variables should be calculated for as NumPy vectors
    :param no_of_training_bags: Integer value of the total amount of training bags
    :param training_bag_vectors: Training bags as Numpy vectors (only needed for prediction)
    :return: Returns a tuple of a list of the random variables and a list containing all summed up distance vectors
    for finding the cause of an anomaly
    """
    sum_of_distances = []
    for bag_i in bag_vectors:
        sum_of_distance = 0
        if training_bag_vectors is not None:
            for bag_j in training_bag_vectors:
                sum_of_distance += np.absolute(bag_i - bag_j)
        else:
            for bag_j in bag_vectors:
                sum_of_distance += np.absolute(bag_i - bag_j)
        sum_of_distances.append(sum_of_distance)
    return list(map(lambda x: np.sum(x) / no_of_bags, sum_of_distances)), sum_of_distances


def calculate_threshold(random_vars, no_of_training_bags, pdf, alpha):
    """
    Calculation of the threshold for determining if there is an anomaly or not

    :param random_vars: Random variables of the training bags
    :param no_of_training_bags: Total amount of training bags
    :param pdf: Estimated probability density function from the random variables
    :param alpha: Sensitivity factor to influence the threshold
    :return: Returns the threshold as float
    """
    sum_of_p_xs = 0
    for x in random_vars:
        sum_of_p_xs += pdf(x)
    return (sum_of_p_xs / no_of_training_bags) * alpha


def find_cause_of_anomaly(sum_of_distances):
    """
    Find the position in the actions array which has the most impact on the random variable of the prediction bag

    :param sum_of_distances[0]: Distance between the prediction bag and the training bags as vector
    :return: Returns the position which has the highest value in distance for the prediction bag
    """
    max_diff = 0
    predsize = len(sum_of_distances[0])
    for i in range(predsize):
        if sum_of_distances[0][i] >= max_diff:
            max_diff = sum_of_distances[0][i]
    pos = []
    for i in range(predsize):
        if sum_of_distances[0][i] == max_diff:
            pos.append(i)
    return pos #  Below line had to replaced because there were problems, thats why it differs from the code in the thesis
    #return [o for o, e in enumerate(sum_of_distances) if e == max_diff]

def contains_anomaly(random_vars, pdf, threshold):
    """
    Checks all random variables from the prediction phase (usually only one) for anomalies

    :param random_vars: Random variables form the prediction phase
    :param pdf: Probability density function estimated in the training phase
    :param threshold: Threshold from the training phase
    :return: Returns True if there is an anomaly and False if not
    """
    for var in random_vars:
        if pdf(var) < threshold:
            print "anomaly! (" + str(pdf(var)) + ") in bag " + str(random_vars.index(var))
            return True
        else:
            print "no anomaly! ( " + str(pdf(var)) + ")"
    return False


def calculate_anomaly_probability(pdf_value, threshold):
    """
    Maps the value of the possible anomaly to the interval between the threshold and zero to estimate
    a false positive probability

    :param pdf_value: Value of the result of the application of the pdf to the random variable of the prediction bag
    :param threshold: Threshold from the training phase
    :return: Returns a float value
    """
    return round((pdf_value * 100) / threshold, 4)


def print_phase_results(bags, random_vars, sum_of_distances, pdf):
    """
    Print the results of a phase (training/prediction)

    :param bags: Corresponding bags
    :param random_vars: Calculated random variables for the bags
    :param sum_of_distances: Summed up distances for each bag
    :param pdf: Probability density function
    :return:
    """
    for i in range(len(bags)):
        print "Bag " + str(i) + ": " + str(bags[i])
    for i in range(len(sum_of_distances)):
        print "SoD of Bag " + str(i) + ": " + str(sum_of_distances[i])
    for i in range(len(random_vars)):
        print "RV of Bag " + str(i) + ": " + str(random_vars[i])
    for i in range(len(random_vars)):
        print "P(x) of Bag " + str(i) + ": " + str(pdf(random_vars[i]))


def training(training_data, alpha):
    """
    Training phase of the Bag of Actions approach

    :param training_data: Extracted training bags from the logs
    :param alpha: Sensitivity factor for the threshold
    :return: Returns a dictionary containing the training bag's vectors, the number of training bags, the threshold theta
    and the estimated probability density function
    """
    # Step 1 - Training: Get Bags of Actions vectors
    training_bags = calculate_bags(training_data)
    training_bags_vectors = list(map(lambda x: np.array(x), training_bags))

    # Step 2 - Training: Calculate random variables x_n
    total_no_of_training_bags = len(training_bags_vectors)  # N
    training_random_vars_calculation = calculate_random_vars(training_bags_vectors, total_no_of_training_bags)
    training_random_vars_vectors = np.array(training_random_vars_calculation[0])

    # Step 3 - Training: Estimate PDF
    try:
        print training_random_vars_vectors
        if len(training_random_vars_vectors) < 2:
            return None
        pdf = gaussian_kde(training_random_vars_vectors, bw_method=0.75 / training_random_vars_vectors.reshape(-1,1).std(ddof=1))
    except LinAlgError:  # If behavior history length < 3 -> singular matrix
        return None

    # Step 4 - Training: Calculate Threshold theta
    theta = calculate_threshold(training_random_vars_calculation[0], total_no_of_training_bags, pdf, alpha)

    # Output - Training
    print "---- Training ----"
    print_phase_results(training_bags, training_random_vars_calculation[0], training_random_vars_calculation[1], pdf)
    print "Threshold: " + str(theta)

    return {'training_bag_vectors': training_bags_vectors, 'no_of_training_bags': total_no_of_training_bags, 'theta': theta, 'pdf': pdf}


def prediction(prediction_data, training_results):
    """
    Prediction phase of the Bag of Actions approach

    :param prediction_data: Extracted prediction bag(s) from the log (usually exactly one)
    :param training_results: Result dictionary of the training phase
    :return: Returns a list containing a bool value at the first position of True if an anomaly was found and False
    if none was found. At the second position the method returns a prediction which position in the actions array was
    responsible for the cause of that anomaly (0 = Logins, 1 = Logouts, etc.)
    """
    # Step 1 - Prediction: Get Vectors of the Bags for prediction logs
    prediction_bags = calculate_bags(prediction_data)
    prediction_bags_vectors = list(map(lambda x: np.array(x), prediction_bags))

    # Step 2 - Prediction: Calculated random vars for prediction bags
    prediction_random_var_calculation = calculate_random_vars(prediction_bags_vectors, training_results['no_of_training_bags'],
                                                              training_results['training_bag_vectors'])

    # Step 3 - Prediction: Try to give a prediction for the cause for a potential anomaly
    positions = find_cause_of_anomaly(prediction_random_var_calculation[1])
    cause = map(lambda x: CONST_ACTIONS[x], positions)

    # Output - Prediction
    print "---- Prediction ----"
    print_phase_results(prediction_bags, prediction_random_var_calculation[0], prediction_random_var_calculation[1],
                        training_results['pdf'])

    # Step 4 - Prediction: Compare values of the prediction bags applied to the PDF with the the threshold
    anomaly_found = contains_anomaly(prediction_random_var_calculation[0], training_results['pdf'], training_results['theta'])

    # Experimental anomaly probability
    anomaly_probability = calculate_anomaly_probability(training_results['pdf'](prediction_random_var_calculation[0][0]), training_results['theta'])
    print "Probability of anomaly: " + str(anomaly_probability)

    return [anomaly_found, anomaly_probability, cause]


def detect(training_data, prediction_data, alpha):
    """
    Training and prediction with Bag of Actions approach

    Parameters
    ----------
    :param training_data: Raw user logs for training as array of string arrays. ([[string log entry,..],..[])
    :param prediction_data: Raw user logs for prediction as array of string arrays. ([[string log entry,..],..[])
    :param alpha: Sensitivity factor for the threshold (higher = more anomalies, lower = less anomalies)

    :return: Returns a list containing a bool value at the first position of True if an anomaly was found and False
    if none was found. At the second position the method returns a prediction which position in the actions array was
    responsible for the cause of that anomaly (0 = Logins, 1 = Logouts, etc.)
    """
    # Training
    training_results = training(training_data, alpha)

    if training_results is None:
        return None

    # Prediction
    prediction_results = prediction(prediction_data, training_results)

    return prediction_results
