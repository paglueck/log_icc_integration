import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formataddr
import email

CONST_MAIL = "mlids@gmx.net"
CONST_MAIL_PW = "l0g1ccm41l"
CONST_MAIL_SERVER = "mail.gmx.net"
CONST_MAIL_PORT = 587


def send_mail_report(timestamp, user, causes, recipients):
    mail_subject = "New anomaly for User " + user + " at " + timestamp
    mail_content = "New anomaly for user " + user + " at " + timestamp + " caused by " + str(causes)

    from_addr = CONST_MAIL
    to_addr = CONST_MAIL

    msg = MIMEText(mail_content)
    msg['To'] = email.utils.formataddr((CONST_MAIL, CONST_MAIL))
    msg['From'] = email.utils.formataddr((CONST_MAIL, CONST_MAIL))
    msg['Subject'] = mail_subject

    server = smtplib.SMTP(CONST_MAIL_SERVER + ":" + str(CONST_MAIL_PORT))
    server.starttls()
    server.login(CONST_MAIL, CONST_MAIL_PW)
    problems = server.sendmail(from_addr, to_addr, msg.as_string())
    server.quit()

    return True
