from . import elastic

def push_logs_to_elastic(index, data):
    """
    Push log data to the elasticsearch cluster - only needed for demonstration purpose rpc - will be removed later

    :param data: Array of log entries
    """
    for entry in data:
        entry_splitted = entry.split(" ")
        timestamp = entry_splitted[0]
        request = strip_brackets(entry_splitted[1], "R")
        ip = strip_brackets(entry_splitted[2], "IP")
        h = strip_brackets(entry_splitted[3], "H")
        s = strip_brackets(entry_splitted[4], "S")
        userid = strip_brackets(entry_splitted[5], "U")
        action = entry_splitted[6]
        details = entry_splitted[7]
        elastic.put_log_entry(index, timestamp, request, ip, h, s, userid, action, details)


def strip_brackets(data, prefix):
    """
    Strip brackets and prefix from a log entry part - e.g.: "IP{123.123.123.123}" -> "123.123.123.123"
    - only needed for demonstration purpose rpc - will be removed later

    Parameters
    ----------
    :param data: String with prefix that should be stripped
    :param prefix: Prefix that should be stripped

    :return: Returns the stripped string of the data
    """

    return data.split(prefix+"{")[1].split("}")[0]