import requests
from datetime import datetime
from time import sleep
from nameko.rpc import rpc, RpcProxy
from . import elastic
from . import utils

# Constant vars - Microservice names
CONST_SERVICE_NAME = "esdata_service"
CONST_SERVICE_NAME_GEN = 'generator_service'

# Constant vars - Elasticsearch cluster address
CONST_ELASTIC_ADDRESS = "http://localhost:9200"

# Constant vars - Test index
CONST_TEST_INDEX = 'icc_demo_logs'

# Constant vars - Query result size
CONST_TEST_SIZE = 10000


def check_elastic_availability():
    """
    Check if elasticsearch is up and running

    :return: Return True if it is running else return false
    """
    try:
        r = requests.get(CONST_ELASTIC_ADDRESS)
    except requests.exceptions.ConnectionError:
        # Elasticsearch not up yet
        return False

    # Elasticsearch is up
    return True


class ElasticDataService:
    """
    Microservice for elasticsearch data pulling(/pushing - only demonstration)
    """

    # Name the microservice reacts while listening for calls
    name = CONST_SERVICE_NAME

    # Necessary microservices that need to be called from this one -- Only for logging/demonstration purposes
    gen = RpcProxy(CONST_SERVICE_NAME_GEN)
    logger = RpcProxy('logging_service')

    @rpc
    def get_historic_data(self, uid, days):
        """
        Fetch the last x days of logs for a a given user ID from the elasticsearch cluster

        :param uid: User's ID
        :param days: Amount of days of data that should be fetched
        :return: Returns the logs as array if successful, False otherwise
        """
        logs = elastic.get_past_days_entries(CONST_TEST_INDEX, uid, days)
        if logs:
            # self.logger.log(CONST_SERVICE_NAME, "ESDATA-GHD-S",
            #                 "Successfully extracted historic data of user %s with data for %d days" % (uid, len(logs)))
            return logs
        # self.logger.log(CONST_SERVICE_NAME, "ESDATA-GHD-S", "Couldn't extract historic data for user %s" % uid)
        return False


    @rpc
    def get_todays_most_recent_entry(self):
        """
        Fetch the most recent entry of today's date

        :return: Returns the log entry if one was found and False if there was none
        """
        data = elastic.get_todays_most_recent_entry(CONST_TEST_INDEX)
        # if data:
        #     self.logger.log(CONST_SERVICE_NAME, "ESDATA-GTRE-S", "Successfully extracted most recent log entry")
        # else:
        #     self.logger.log(CONST_SERVICE_NAME, "ESDATA-GTRE-E", "Couldn't extract most recent log entry (No entry existing for current day")
        return data

    @rpc
    def get_todays_entries_between_timestamps(self, from_timestamp, to_timestamp):
        """
        Get all log entries from today starting from a given timestamp

        :param from_timestamp: Timestamp for the starting position for the log extraction
        :param to_timestamp: Timestamp for the ending position for the log extraction
        :return: Returns a list with all the log entries that have been found
        """
        data = elastic.get_todays_entries_between_timestamps(CONST_TEST_INDEX, 10000, from_timestamp, to_timestamp)
        # if data:
        #     self.logger.log(CONST_SERVICE_NAME,
        #                     "ESDATA-GTEBT-S", "Successfully extracted log entries between timestamps %s and %s"
        #                     % (from_timestamp, to_timestamp))
        # else:
        #     self.logger.log(CONST_SERVICE_NAME,
        #                     "ESDATA-GTEBT-E", "No log entries were extracted between timestamps %s and %s"
        #                     % (from_timestamp, to_timestamp))
        return data

    @rpc
    def fetch_todays_entries(self, userid):
        data = elastic.fetch_todays_entries(CONST_TEST_INDEX, userid)
        # if data:
        #     self.logger.log(CONST_SERVICE_NAME, "ESDATA-FTE-S",
        #                     "Successfully extracted log entries of user %s for the current day with %d entries"
        #                     % (userid, len(data)))
        # else:
        #     self.logger.log(CONST_SERVICE_NAME, "ESDATA-FTE-E",
        #                     "No log entries were extracted for user %s for the current day" % userid)
        return data

    @rpc
    def get_todays_entries(self):
        """
        Get all log entries for today

        :return: Returns the log entries if found, False otherwise
        """
        data = elastic.get_todays_entries(CONST_TEST_INDEX, 10000)
        return data

    # ------------------------------------------------- #
    #  DEMONSTRATION/EVALUATION PURPOSE RPCs            #
    # ------------------------------------------------- #

    @rpc
    def fetch_days_entries(self, userid, day):
        data = elastic.fetch_days_entries(CONST_TEST_INDEX, userid, day)
        # if data:
        #     self.logger.log(CONST_SERVICE_NAME, "ESDATA-FTE-S",
        #                     "Successfully extracted log entries of user %s for day %s with %d entries"
        #                     % (userid, day, len(data)))
        # else:
        #     self.logger.log(CONST_SERVICE_NAME, "ESDATA-FTE-E",
        #                     "No log entries were extracted for user %s for day %s" % (userid, day))
        return data

    @rpc
    def get_historic_data_v2(self, uid, days, start_day):
        """
        Fetch the last x days of logs for a a given user ID from the elasticsearch cluster

        :param uid: User's ID
        :param days: Amount of days of data that should be fetched
        :return: Returns the logs as array if successful, False otherwise
        """
        logs = elastic.get_past_days_entries_v2(CONST_TEST_INDEX, uid, days, start_day)
        if logs:
            # self.logger.log(CONST_SERVICE_NAME, "ESDATA-GHD-S",
            #                 "Successfully extracted historic data of user %s with data for %d days starting from day %s" % (uid, len(logs), start_day))
            return logs
        # self.logger.log(CONST_SERVICE_NAME, "ESDATA-GHD-E", "Couldn't extract historic data for user %s and start day %s" % (uid, start_day))
        return False

    @rpc
    def demo_data_upstream(self, num_of_users):
        """
        Pushes demo data to the elasticsearch cluster

        :param num_of_users: Number of users for which data should be pushed to the elasticsearch cluster

        :return: Returns True if it was successful, False otherwise
        """
        if check_elastic_availability():
            print "Generating historic user data...."
            historical_data = self.gen.generate_historical_data(num_of_users, -7)
            print "Uploading historic data to elasticsearch..."
            utils.push_logs_to_elastic(CONST_TEST_INDEX, historical_data)
            return True
        return False

    @rpc
    def upstream_log_entries_array(self, entries):
        """
        Push an array of log entries to the elasticsearch cluster

        :param entries: Log entries in an array

        :return: Returns True if successful, False otherwise
        """
        utils.push_logs_to_elastic(entries)
        return True

    @rpc
    def delete_todays_entries(self):
        """
        Delete all entries of the current day

        :return: # TODO: Return
        """
        return elastic.delete_todays_entries(CONST_TEST_INDEX, 10000)

    @rpc
    def clean_index(self):
        """
        Clears the specified index

        :return: Returns the result of the elasticsearch query if successful and "Index not found" if the index doesn't
        exist
        """
        return elastic.clean_index(CONST_TEST_INDEX)

    @rpc
    def start_simulating_usage(self):
        """
        Generates behavior data for the current day for all users in the elasticsearch cluster with data for the last
        week and pushes it to the elasticsearch cluster in chunks - Simulates the daily usage and will be picked up by
        the monitoring microservice
        """
        ips = elastic.get_last_weeks_ips(CONST_TEST_INDEX)
        todays_behavior = self.gen.generate_usage_for_today(ips)
        parts = len(todays_behavior) / 5
        chunks = [todays_behavior[i:i + parts] for i in xrange(0, len(todays_behavior), parts)]
        for chunk in chunks:
            print("Pushing new behavior")
            utils.push_logs_to_elastic(CONST_TEST_INDEX, chunk)
            sleep(5)
