from elasticsearch import Elasticsearch
from elasticsearch import helpers
from datetime import datetime, timedelta
CONST_TEST_DOC_TYPE = 'log_entry'

# TODO: REFACTOR METHODS AND COMMENT


def get_todays_entries(index, size):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        res = es.search(index=index, doc_type=CONST_TEST_DOC_TYPE, body={"query": {"range" : {"timestamp" : {"gte" : "now/d", "lt": "now/d+1d"}}}, "size": size, "sort": [{"timestamp": {"order": "asc"}}]})
        print("%d documents found" % res['hits']['total'])
        if res['hits']['total'] == 0:
            return False
        logs = []
        for doc in res['hits']['hits']:
            logs.append(rebuild_log_entry_string(doc['_source']))
        return logs
    else:
        return "Index not found"


def get_todays_entries_between_timestamps(index, size, from_timestamp, to_timestamp):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        query = {"query": {"range" : {"timestamp" : {"gte" : datetime.strptime(from_timestamp, '%Y-%m-%dT%H:%M:%SZ'), "lte": datetime.strptime(to_timestamp, '%Y-%m-%dT%H:%M:%SZ')}}}, "size": size, "sort": [{"timestamp": {"order": "asc"}}]}
        res = es.search(index=index, doc_type=CONST_TEST_DOC_TYPE, body=query)
        print("%d documents found" % res['hits']['total'])
        logs = []
        for doc in res['hits']['hits']:
            logs.append(rebuild_log_entry_string(doc['_source']))
        return logs
    else:
        return False


def get_todays_most_recent_entry(index):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        res = es.search(index=index, doc_type=CONST_TEST_DOC_TYPE, body={"query": {"range" : {"timestamp" : {"gte" : "now/d", "lt": "now/d+1d"}}}, "size": 1, "sort": [{"timestamp": {"order": "desc"}}]})
        print("%d documents found" % res['hits']['total'])
        if res['hits']['total'] == 0:
            return False
        logs = []
        for doc in res['hits']['hits']:
            return rebuild_log_entry_string(doc['_source'])
        #return logs
    else:
        return "Index not found"


def fetch_todays_entries(index, uid):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        res = es.search(index=index, doc_type=CONST_TEST_DOC_TYPE, body={"size": 10000, "query":{ "bool": {"must": {"term": {"U": uid}},"filter": {"range":{"timestamp":{"gte": "now/d", "lt": "now/d+1d"}}}}}, "sort": [{"timestamp": {"order": "asc"}}]})
        print("%d documents found" % res['hits']['total'])
        logs = []
        for doc in res['hits']['hits']:
            logs.append(rebuild_log_entry_string(doc['_source']))
        return logs
    else:
        return "Index not found"


def fetch_days_entries(index, uid, day):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        day_date_start = datetime.strptime(day, '%Y-%m-%dT%H:%M:%SZ')
        day_date_start2 = datetime.strptime(day, '%Y-%m-%dT%H:%M:%SZ').replace(hour=0, minute=0, second=0, microsecond=0)
        res = es.search(index=index, doc_type=CONST_TEST_DOC_TYPE, body={"size": 10000, "query":{ "bool": {"must": {"term": {"U": uid}},"filter": {"range":{"timestamp":{"gte": day_date_start2, "lte": day_date_start}}}}}, "sort": [{"timestamp": {"order": "asc"}}]})
        print("%d documents found" % res['hits']['total'])
        logs = []
        for doc in res['hits']['hits']:
            logs.append(rebuild_log_entry_string(doc['_source']))
        return logs
    else:
        return "Index not found"


def get_last_weeks_entries(index, uid):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        res = es.search(index=index, doc_type=CONST_TEST_DOC_TYPE, body={"size": 10000, "query":{ "bool": {"must": {"term": {"U": uid}},"filter": {"range":{"timestamp":{"gt": "now/d-8d", "lt": "now/d"}}}}}, "sort": [{"timestamp": {"order": "asc"}}]})
        print("%d documents found" % res['hits']['total'])
        logs = []
        for doc in res['hits']['hits']:
            logs.append(rebuild_log_entry_string(doc['_source']))
        return logs
    else:
        return "Index not found"


def get_past_days_entries(index, uid, days):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        logs = []
        days_entries_found = days_searched = 0
        timeout = 14
        while days_entries_found < days:
            if days_searched == timeout or days_entries_found == timeout:
                return logs
            query = {"query": {"bool": {"must": {"term": {"U": uid}}, "filter": {"range": {"timestamp": {"gte": "now/d-" + str(days_searched+1) + "d", "lt": "now/d-" + str(days_searched) + "d"}}}}},
                     "size": 10000,
                     "sort": [{"timestamp": {"order": "asc"}}]}
            result = es.search(index=index, doc_type=CONST_TEST_DOC_TYPE, body=query)
            result_entries = result['hits']['hits']
            if result_entries:
                logs.append(map(lambda x: rebuild_log_entry_string(x['_source']), result_entries))
                days_entries_found += 1
            days_searched += 1
        return logs[::-1]
    return False


def get_past_days_entries_v2(index, uid, days, start_day):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        logs = []
        days_entries_found = days_searched = 0
        timeout = 14
        start_day_date_start = datetime.strptime(start_day, '%Y-%m-%dT%H:%M:%SZ').replace(hour=0, minute=0, second=0, microsecond=0)
        start_day_date_end = datetime.strptime(start_day, '%Y-%m-%dT%H:%M:%SZ').replace(hour=23, minute=59, second=59, microsecond=999)
        while days_entries_found < days:
            if days_searched == timeout or days_entries_found == timeout:
                return logs[::-1]
            query = {"query": {"bool": {"must": {"term": {"U": uid}}, "filter": {"range": {"timestamp": {"gte": start_day_date_start - timedelta(days=days_searched+1), "lt": start_day_date_start - timedelta(days=days_searched)}}}}},
                     "size": 10000,
                     "sort": [{"timestamp": {"order": "asc"}}]}
            result = es.search(index=index, doc_type=CONST_TEST_DOC_TYPE, body=query)
            result_entries = result['hits']['hits']
            if result_entries:
                logs.append(map(lambda x: rebuild_log_entry_string(x['_source']), result_entries))
                days_entries_found += 1
            days_searched += 1
        return logs[::-1]
    return False


def get_log_list(query_result):
    log = []
    for entry in query_result:
        log.append(rebuild_log_entry_string(entry['_source']))


def rebuild_log_entry_string(elastic_source):
    log_entry = elastic_source['timestamp'] + " R{" + elastic_source['R'] + "} IP{" + elastic_source['IP'] + "} H{" + str(elastic_source['H']) + "} S{"+ str(elastic_source['S']) + "} U{" + elastic_source['U'] + "} " + elastic_source['action'] + " " + elastic_source['details']
    return log_entry


# ------------------------------------------------- #
#            DEMONSTRATION PURPOSE METHODS          #
# ------------------------------------------------- #


def get_last_weeks_ips(index):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        res = es.search(index=index, doc_type=CONST_TEST_DOC_TYPE, body={"size": 10000, "_source": ["IP"], "query":{"range":{"timestamp":{"gt": "now/d-8d", "lt": "now/d"}}}, "sort": [{"timestamp": {"order": "asc"}}]})
        print("%d documents found" % res['hits']['total'])
        ips = []
        for doc in res['hits']['hits']:
            ips.append((doc['_source']))
        return list(set(map(lambda x : x['IP'], ips)))
    else:
        return "Index not found"


def put_log_entries_bulk(entries):
    es = Elasticsearch()
    print(helpers.bulk(es, entries))


def put_log_entry(index, timestamp, request, ip, status, s, userid, action, details):
    """
    Pushes a log entry to the elasticsearch cluster - only for demonstration purposes needed

    :param index: Index in the elasticsearch cluster
    :param timestamp: Timestamp of the log entry
    :param request:  Request-ID of the log entry
    :param ip: IP address of the log entry
    :param status: Status code of the answer of the log entry
    :param s: http status code of the log entry
    :param userid: User ID of the log entry
    :param action: Executed action of the log entry
    :param details: Details of the action of the log entry

    :return: Returns the result of the elasticsearch query
    """
    es = Elasticsearch()
    entry = {
        'timestamp': timestamp,
        'R': request,
        'IP': ip,
        'H': status,
        'S': s,
        'U': userid,
        'action': action,
        'details': details,
    }
    res = es.index(index=index, doc_type=CONST_TEST_DOC_TYPE, body=entry)
    print(res['result'])


def delete_todays_entries(index, size):
    es = Elasticsearch()
    if es.indices.exists(index=index):
        res = es.delete_by_query(index=index, doc_type=CONST_TEST_DOC_TYPE, body={"query": {"range": {"timestamp": {"gte": "now/d", "lt": "now/d+1d"}}}, "size": size,"sort": [{"timestamp": {"order": "asc"}}]})
        return res
    else:
        return "Index not found"


def clean_index(index):
    """
    Clears the specified index of all data, if it exists - Only for demonstration purposes needed

    :param index: The index that should be cleaned

    :return: Returns the result of the query if the index exists, False otherwise
    """
    es = Elasticsearch()
    if es.indices.exists(index=index):
        return es.indices.delete(index=index, ignore=[400, 404])
    else:
        return False
